<?php

namespace Drupal\commerce_pei\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the PEI payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "pei",
 *   label = @Translation("PEI"),
 *   create_label = @Translation("New PEI payment with phone"),
 * )
 */
class PeiPaymentMethodType extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return $this->t('PEI payment with phone @phone', ['@phone' => $payment_method->telephone->value]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['telephone'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Telephone number'))
      ->setDescription($this->t('The telephone number.'))
      ->setRequired(TRUE);

    $fields['issn'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('ISSN number'))
      ->setDescription($this->t('The ISSN number.'))
      ->setRequired(TRUE);

    $fields['pincode'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Pin code'))
      ->setDescription($this->t('The Pin code.'))
      ->setRequired(TRUE);

    return $fields;
  }

}
