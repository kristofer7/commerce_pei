<?php

namespace Drupal\commerce_pei;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;

class PeiApi {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * The Guzzle HTTP client.
   *
   * @var \GuzzleHttp\Client;
   */
  protected $client;

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * Constant indicating error code for an undefined parameter.
   *
   * @var integer
   */
  const UNDEFINED_PARAM = 0;

  /**
   * Constant indicating error code for a bad parameter.
   *
   * @var integer
   */
  const BAD_PARAM = 2;

  /**
   * Constant indicating the live environment.
   *
   * @var string
   */
  const PEI_URL_LIVE = 'https://api.pei.is';

  /**
   * Constant indicating the test environment.
   *
   * @var string
   */
  const PEI_URL_TEST = 'https://externalapistaging.pei.is';

  /**
   * Constant indicating the live environment token endpoint.
   *
   * @var string
   */
  const PEI_URL_LIVE_TOKEN_ENDPOINT = 'https://auth.pei.is/core/connect/token';

  /**
   * Constant indicating the test environment token endpoint.
   *
   * @var string
   */
  const PEI_URL_TEST_TOKEN_ENDPOINT = 'https://authstaging.pei.is/core/connect/token';

  /**
   * Constant indicating the authentication failed.
   */
  const AUTHENTICATION_FAILED = FALSE;

  /**
   * Constant indicating authentication was requested and a pin code was sent.
   */
  const AUTHENTICATION_REQUESTED = 1;

  /**
   * Constant indicating authentication was granted.
   */
  const AUTHENTICATION_GRANTED = 2;

  /**
   * The Authentication access token.
   *
   * @var string
   */
  private $accessToken;

  /**
   * The merchant ID.
   *
   * @var string
   */
  private $merchantId;

  /**
   * Environment: live or test.
   *
   * @var string
   */
  private $environment;

  /**
   * Constructs a new PeiApi object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP client.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  public function __construct(ClientInterface $http_client, TranslationInterface $string_translation) {
    $this->client = $http_client;
    $this->stringTranslation = $string_translation;
  }

  /**
   * Gets the current environment.
   *
   * @return string
   *   Environment. Can be 'live' or 'test'.
   */
  public function getEnvironment() {
    return $this->environment;
  }

  /**
   * Set the environment.
   *
   * @param string $environment
   *   Environment. Can be 'live' or 'test'.
   * @return $this
   */
  public function setEnvironment($environment) {
    $this->environment = $environment;
    return $this;
  }

  /**
   * Gets the environment URL.
   */
  public function getEnvironmentURL() {
    $environment_url = self::PEI_URL_TEST;

    if ($this->getEnvironment() == 'live') {
      $environment_url = self::PEI_URL_LIVE;
    }

    return $environment_url;
  }

  /**
   * Gets the token endpoint.
   */
  public function getTokenEndpoint() {
    $token_endpoint = self::PEI_URL_TEST_TOKEN_ENDPOINT;

    if ($this->getEnvironment() == 'live') {
      $token_endpoint = self::PEI_URL_LIVE_TOKEN_ENDPOINT;
    }

    return $token_endpoint;
  }

  /**
   * Authenticates.
   *
   * @param string $environment
   *   The environment.
   * @param string $merchant_id
   *   The merchant ID.
   * @param string $client_id
   *   The client ID.
   * @param string $secret
   *   The secret
   *
   * @return bool
   *   TRUE if authenticated successfully. FALSE otherwise.
   */
  public function authenticate($environment, $merchant_id, $client_id, $secret) {
    $result = FALSE;
    $this->setEnvironment($environment);
    $this->merchantId = $merchant_id;

    $tokenEndpoint = $this->getTokenEndpoint();

    $tokenResponse = $this->client->post($tokenEndpoint, [
      RequestOptions::AUTH => [$client_id, $secret],
      RequestOptions::HEADERS => [
        'Accept' => 'application/json',
        'Content-Type' => 'application/x-www-form-urlencoded',
      ],
      RequestOptions::FORM_PARAMS => [
        'grant_type' => 'client_credentials',
        'scope' => 'externalapi',
      ],
      'http_errors' => FALSE,
    ]);

    if ($tokenResponse->getStatusCode() === 200) {
      $result = TRUE;
      $json = Json::decode($tokenResponse->getBody());

      $access_token = $json['access_token'];
      $expires_in = $json['expires_in'];
      $tokenType = $json['token_type'];
      $result = $this->accessToken = $access_token;
    }
    else {
      $statusCode = $tokenResponse->getStatusCode();
      $json = Json::decode($tokenResponse->getBody());
      $errorMessage = $json['error'];
      // Log this: drupal_set_message('An error happened with code ' . $statusCode . '. Please see the error message: ' . $errorMessage, 'error');
    }
    return $result;
  }

  /**
   * Submits an order to Payment Portal (Greiðslugátt).
   */
  public function submitOrder(PaymentInterface $payment) {
    $data = [];

    $order = $payment->getOrder();
    $buyer = $order->getBillingProfile();
    $address = $buyer->get('address')->getValue();

    // ToDo: There must be a better way.
    $name = $address[0]['given_name'] . ' ' . $address[0]['family_name'];

    $paymentMethod = $payment->getPaymentMethod();
    $issn = $paymentMethod->get('issn')->value;
    $mobileNumber = $paymentMethod->get('telephone')->value;

    $data['merchantId'] = $this->merchantId;
    $data['buyer'] = [
      'name' => $name,
      'ssn' => $issn,
      'email' => $order->getEmail(),
      'mobileNumber' => $mobileNumber,
    ];
    $data['amount'] = $payment->getAmount()->getNumber();
    $data['reference'] = $order->id();
    $data['successReturnUrl'] = '';
    $data['cancelReturnUrl'] = '';
    $data['postbackUrl'] = '';
    $data['items'] = [];

    $items = $order->getItems();
    foreach ($items as $item) {
      $purchasedEntity = $item->getPurchasedEntity();
      $data['items'][] = [
        'code' => $purchasedEntity->getSku(),
        'name' => $item->getTitle(),
        'quantity' => $item->getQuantity(),
        'unit' => 'units',
        'unitPrice' => $item->getUnitPrice()->getNumber(),
        'amount' => $item->getAdjustedTotalPrice()->getNumber(),
      ];
    }

    $result = FALSE;
    $endpoint = $this->getEnvironmentURL() . '/api/orders/pay';

    $response = $this->client->post($endpoint, [
      RequestOptions::HEADERS => [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
        'Authorization' => 'bearer ' . $this->accessToken,
      ],
      RequestOptions::JSON => $data,
      'http_errors' => FALSE,
    ]);

    if ($response->getStatusCode() === 200) {
      $json = Json::decode($response->getBody());
      $result = $orderId = $json['orderId'];
    }
    else {
      $statusCode = $response->getStatusCode();
      $errorMessage = Json::decode($response->getBody());
    }
    return $result;
  }

  public function checkAccessToBuyer($buyer_ssn) {
    $result = FALSE;
    $endpoint = $this->getEnvironmentURL() . '/api/purchaseaccess';

    $response = $this->client->get($endpoint, [
      RequestOptions::QUERY => [
        'merchantId' => $this->merchantId,
        'buyerSsn' => $buyer_ssn,
      ],
      RequestOptions::HEADERS => [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
        'Authorization' => 'bearer ' . $this->accessToken,
      ],
      'http_errors' => FALSE,
    ]);

    if ($response->getStatusCode() === 200) {
      $result = Json::decode($response->getBody());
    }
    else {
      $statusCode = $response->getStatusCode();
      $json = Json::decode($response->getBody());
      $errorMessage = $json['message'];
    }
    return $result;
  }

  public function requestAccessToBuyer($buyer_ssn, $mobile_number) {
    $result = FALSE;
    $endpoint = $this->getEnvironmentURL() . '/api/purchaseaccess/request';

    $data = [
      'merchantId' => $this->merchantId,
      'buyerSsn' => $buyer_ssn,
      'mobileNumber' => $mobile_number,
    ];

    $response = $this->client->post($endpoint, [
      RequestOptions::JSON => $data,
      RequestOptions::HEADERS => [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
        'Authorization' => 'bearer ' . $this->accessToken,
      ],
      'http_errors' => FALSE,
    ]);

    if ($response->getStatusCode() === 200) {
      $content = $response->getBody()->getContents();
      // $content can be "This merchant can already purchase orders for this buyer."
      if ($content === '"This merchant can already purchase orders for this buyer."') {
        $result = static::AUTHENTICATION_GRANTED;
      }
      else {
        $result = static::AUTHENTICATION_REQUESTED;
      }
    }
    else {
      $statusCode = $response->getStatusCode();
      $json = Json::decode(Json::decode($response->getBody()));

      // {"code":"10002","message":"Kennitala test fannst ekki í þjóðskrá."}
      // {"code":"10006","message":"Mobile number not trusted for this ssn."}
      $code = isset($json['code']) ? $json['code'] : NULL;
      $errorMessage = $json['message'];
    }
    return $result;
  }

  public function confirmAccessToBuyer($buyer_ssn, $pin) {
    $result = FALSE;

    $endpoint = $this->getEnvironmentURL() . '/api/purchaseaccess';

    $data = [
      'merchantId' => $this->merchantId,
      'buyerSsn' => $buyer_ssn,
      'pin' => $pin,
    ];

    $response = $this->client->post($endpoint, [
      RequestOptions::JSON => $data,
      RequestOptions::HEADERS => [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
        'Authorization' => 'bearer ' . $this->accessToken,
      ],
      'http_errors' => FALSE,
    ]);

    if ($response->getStatusCode() === 200) {
      $result = static::AUTHENTICATION_GRANTED;
    }
    else {
      $statusCode = $response->getStatusCode();
      /***
       * 400 Post body was invalid.
       * 403 Failed to validate buyer pin.
       * 404 Buyer not found.
       */
      $content = $response->getBody()->getContents();
      // E.g. 'Buyer not found.'
      $errorMessage = Json::decode(Json::decode($content));
    }
    return $result;
  }

}
